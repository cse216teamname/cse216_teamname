<div id="EditUserProfile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Profile</h4>
            </div>
            <div class="modal-body">
                <label for="EditUserProfile-username">Username</label>
                <input class="form-control" type="text" id="EditUserProfile-username" />
                <label for="EditUserProfile-intro">Intro</label>
                <textarea class="form-control" id="EditUserProfile-intro"></textarea>
                <button type="button" class="btn btn-default" id="EditUserProfile-Update">Update</button>

                <label for="EditUserProfile-pwd">Password</label>
                <textarea class="form-control" id="EditUserProfile-pwd"></textarea>
                <button type="button" class="btn btn-default" id="EditUserProfile-Reset">Reset</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="EditUserProfile-Cancel">Cancel</button>
            </div>
        </div>
    </div>
</div>
