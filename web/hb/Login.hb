<div id="Login" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">User Login</h4>
            </div>
            <div class="modal-body">
                <label for="Login-ID">User ID</label>
                <input class="form-control" type="text" id="Login-ID" />
                <label for="Login-pwd">Password</label>
                <input class="form-control" type="password" id="Login-pwd"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="Login-Login">Login</button>
                <button type="button" class="btn btn-default" id="Login-Close">Close</button>
                <button type="button" class="btn btn-default" id="Login-ll">ll</button>
            </div>
        </div>
    </div>
</div>