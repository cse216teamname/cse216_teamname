<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" 
                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- Clicking the brand refreshes the page -->
            <a class="navbar-brand" href="/">Buzz</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a class="btn btn-link" id="Navbar-add">
                        <span class="glyphicon glyphicon-plus"></span><span class="sr-only">Show Trending Posts</span>
                        Post an Event
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="btn btn-link" id="Navbar-Account">
                        <span class="glyphicon glyphicon-user"></span> Account
                    </a>
                </li>
                <li>
                    <a class="btn btn-link" id="Navbar-login">
                        <span class="glyphicon glyphicon-log-in"></span><span class="sr-only">Show</span>
                        Login
                    </a>
                </li>
                <li>
                    <a class="btn btn-link" id="Navbar-showQR">
                    <span class="glyphicon glyphicon-picture"></span><span class="sr-only">Show Trending Posts</span>
                        View QR code
                        <canvas id="canvas"></canvas>
                    </a>
                    <canvas id="canvas"></canvas>
                </li>
            </ul>
        </div>
    </div>
</nav>