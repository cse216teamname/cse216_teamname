<div id="UserProfile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">User Profile</h4>
            </div>
            <div class="modal-body">
                <label for="UserProfile-name">User name</label>
                <span id="UserProfile-name"></span>
                <label for="UserProfile-email">User Email</label>
                <span id="UserProfile-email"></span>
                <label for="UserProfile-intro">Details</label>
                <textarea class="form-control" id="UserProfile-intro"></textarea>
                <input type="hidden" id="UserProfile-detailId" />
                <label for="UserProfile-title">Message title</label>
                <span id="UserProfile-title"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="UserProfile-editbtn">Edit</button>
                <button type="button" class="btn btn-default" id="UserProfile-Close">Close</button>
            </div>
        </div>
    </div>
</div>