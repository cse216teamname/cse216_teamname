<div id="NewEntryForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Post a New Event</h4>
            </div>
            <div class="modal-body">
                <label for="NewEntryForm-title">Event Title</label>
                <input class="form-control" type="text" id="NewEntryForm-title" />
                <label for="NewEntryForm-message">Event Detail</label>
                <textarea class="form-control" id="NewEntryForm-message"></textarea>
                <label for="NewEntryForm-link">Link</label>
                <input class="form-control" type="text" id="NewEntryForm-link" />
                <label for="NewEntryForm-attachment">Attachment</label>
                <input class="form-control-file" type="file" accept=".pdf" id="NewEntryForm-attachment" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="NewEntryForm-OK">OK</button>
                <button type="button" class="btn btn-default" id="NewEntryForm-Close">Close</button>
            </div>
        </div>
    </div>
</div>